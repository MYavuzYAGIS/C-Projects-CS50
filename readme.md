# Weekly Assignments of Harvard's CS50

# SOLUTIONS FOR C SECTION

## DISCLAIMER:
### I opted for MVP, Once a code is working as it should, I did not deal with refactoring or rewriting some elements in order to achieve elegancy since the trade-off meta, time, is more valuable now my case.!
<br> </br>
### Thus, these codes should be considered as beta versions, not necessarily display the quality of possible final product!. 


<br> </br>

## 1) Mario:

![mario](img/mario.png)

## 2) Caesar:

![mario](img/caesar.png)

## 3) Text Readability Algorithm.

![mario](img/read2.png)

## Another Screensot for Readability.

<br> </br>

![mario](img/read.png)


## 4) Plurality - A basic Voting system

![mario](img/plur.png)
<br> </br>

## 5) Filtering

<br> </br>

#### Original Picture

![mario](W4Filter/images/stadium.bmp)

#### a) filter- Greyscale

<br> </br>

![mario](img/filter1.bmp)

<br> </br>

#### b) filter- Sepia

![mario](img/filter2.bmp)

<br> </br>
#### c) filter- Reverse
![mario](img/filter3.bmp)


<br> </br>

## 5) Recover Data from Forensic Image

<br> </br>

#### Original Picture

![mario](img/forensic.png)



## FINALLY!!

## 6) SPELLER

<br> </br>

When N=26 in Linked List Bucket size
![mario](img/N26.png)


When N = 2000000 in linked list bucket Size
<br> </br>
![mario](img/N2000000.png)



